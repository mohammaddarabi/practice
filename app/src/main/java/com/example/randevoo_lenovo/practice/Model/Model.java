package com.example.randevoo_lenovo.practice.Model;

import android.arch.persistence.room.Room;

import com.example.randevoo_lenovo.practice.MainInterface.MainInterface;
import com.example.randevoo_lenovo.practice.Models.User;


/**
 * Created by randevoo on 9/11/2018.
 */

public class Model implements MainInterface.ModelOpsForPresenter{

    private MainInterface.PresenterForModel mPresenter ;
    private DataBase mDataBase ;

    public Model(MainInterface.PresenterForModel mPresenter) {
        this.mPresenter = mPresenter;
        mDataBase = Room.databaseBuilder(mPresenter.getAppContext() , DataBase.class , "UsersDB")
                .build() ;
    }

    @Override
    public long inserNewUser(User user) {
        return mDataBase.getUserOps().insertUser(user)[user.getId()];
    }

    @Override
    public User getUser(String name, String userName) {
        return mDataBase.getUserOps().getUser(name , userName);
    }
}
