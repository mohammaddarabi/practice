package com.example.randevoo_lenovo.practice.View;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.randevoo_lenovo.practice.MainInterface.MainInterface;
import com.example.randevoo_lenovo.practice.Presenter.Presenter;
import com.example.randevoo_lenovo.practice.R;


public class MainActivity extends AppCompatActivity implements MainInterface.ViewOpsForPresenter
        , View.OnClickListener {


    private EditText mName;
    private EditText mBirthDay;
    private EditText mUserName;
    private EditText mEmail;
    private EditText mPhoneNumber;
    private Button mSave;
    private Button mLoadData;
    private MainInterface.PresenterOpsForView mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        setUpMVP();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        setUpMVP();
    }

    private void initView() {
        mName = findViewById(R.id.edt_name);
        mBirthDay = findViewById(R.id.edt_birth_day);
        mUserName = findViewById(R.id.edt_user_name);
        mEmail = findViewById(R.id.edt_email);
        mPhoneNumber = findViewById(R.id.edt_phone_number);
        mSave = findViewById(R.id.btn_save);
        mLoadData = findViewById(R.id.btn_load);
        mSave.setOnClickListener(this);
        mLoadData.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_save) {
            String name = mName.getText().toString();
            String birhtDay = mBirthDay.getEditableText().toString();
            String userName = mUserName.getText().toString();
            String email = mEmail.getText().toString();
            String phone = mPhoneNumber.getText().toString();
            mPresenter.saveUser(name, birhtDay, userName, email, phone);
        } else if (v.getId() == R.id.btn_load)
            startActivity(new Intent(this, LoadActivity.class));
    }

    private void setUpMVP() {
        mPresenter = new Presenter(this);
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }
}
