package com.example.randevoo_lenovo.practice.MainInterface;

import android.content.Context;

import com.example.randevoo_lenovo.practice.Models.User;

/**
 * Created by randevoo on 9/11/2018.
 */

public interface MainInterface {


    interface ViewOpsForPresenter {
        void showToast(String s);
        Context getAppContext();
    }

    interface ModelOpsForPresenter {
        long inserNewUser(User user);
        User getUser(String name, String userName) ;
    }

    interface PresenterOpsForView {
        void saveUser(String name, String birhtDay, String userName, String email, String phone);
        void getUser(String name, String userName) ;
    }

    interface PresenterForModel {
        Context getAppContext();
    }

    interface LoadViewOpsToPresenter{
        void showToast(String message) ;
        void setText(String phone, String userBirthDay, String email);
        Context getAppContext();
    }
}

