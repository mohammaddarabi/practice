package com.example.randevoo_lenovo.practice.Presenter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


import com.example.randevoo_lenovo.practice.MainInterface.MainInterface;
import com.example.randevoo_lenovo.practice.Model.Model;
import com.example.randevoo_lenovo.practice.Models.User;

import java.lang.ref.WeakReference;
import java.security.MessageDigest;

/**
 * Created by randevoo on 9/11/2018.
 */

public class Presenter implements MainInterface.PresenterForModel, MainInterface.PresenterOpsForView {

    private WeakReference<MainInterface.ViewOpsForPresenter> mView;
    private WeakReference<MainInterface.LoadViewOpsToPresenter> mLoadView;
    private Model mModel;
    private SecretUtils mSecretUtils;
    private Context mContext;
//    String s;

    public Presenter(MainInterface.ViewOpsForPresenter view) {
        mView = new WeakReference<>(view);
        mContext = getView().getAppContext();
        setUpModel();
    }

    public Presenter(MainInterface.LoadViewOpsToPresenter view) {
        mLoadView = new WeakReference<>(view);
        mContext = getLoadView().getAppContext();
        setUpModel();
    }

    private void setUpModel() {
        mModel = new Model(this);
    }

    private String digest(String input) throws Exception {
        StringBuilder outPut = new StringBuilder();
        MessageDigest mMessageDigest = MessageDigest.getInstance("SHA-512");
        mMessageDigest.update(input.getBytes());
        return new String(mMessageDigest.digest());
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void saveUser(final String name, final String birhtDay, final String userName, final String email, final String phone) {
        new AsyncTask<Void, Void, Long>() {

            byte[] enName, enBirthDay, enUserName, enEmail, enPhone = null;

            @Override
            protected Long doInBackground(Void... voids) {
                String hashName = null;
                String hashUserName = null;
                try {
                    mSecretUtils = new SecretUtils(getAppContext(), userName);
                    enName = mSecretUtils.encrypt(name);
                    enBirthDay = mSecretUtils.encrypt(birhtDay);
                    enUserName = mSecretUtils.encrypt(userName);
                    enEmail = mSecretUtils.encrypt(email);
                    enPhone = mSecretUtils.encrypt(phone);
                    hashName = digest(name);
                    hashUserName = digest(userName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return mModel.inserNewUser(new User(hashName, hashUserName, enName, enBirthDay, enUserName, enEmail, enPhone));
            }

            @Override
            protected void onPostExecute(Long position) {
                try {
                    if (position > -1)
                        getView().showToast("that user inserted successfully in position : " + position);
                    else
                        getView().showToast("unexpected error !");
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                super.onPostExecute(position);
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void getUser(final String inputName, final String inputUserName) {
        new AsyncTask<Void, Void, User>() {

            String name, userName, phone, birthDay, email = null;

            @Override
            protected User doInBackground(Void... voids) {
                User user = null;
                try {
                    user = mModel.getUser(digest(inputName), digest(inputUserName));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return user;
            }

            @Override
            protected void onPostExecute(User user) {
                super.onPostExecute(user);
                if (user != null) {
                    try {
                        mSecretUtils = new SecretUtils(getAppContext(), inputUserName);
                        name = mSecretUtils.decrypt(user.getName());
                        userName = mSecretUtils.decrypt(user.getUserName());
                        birthDay = mSecretUtils.decrypt(user.getUserBirthDay());
                        email = mSecretUtils.decrypt(user.getEmail());
                        phone = mSecretUtils.decrypt(user.getPhone());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    getLoadView().setText(phone, birthDay, email);
                    Log.i("test", "----name : " + name);
                    Log.i("test", "----userName : " + userName);
                    Log.i("test", "----birthDay : " + birthDay);
                    Log.i("test", "----email : " + email);
                    Log.i("test", "----phone : " + phone);
                } else
                    getLoadView().showToast("Password or Name is`t match !");
            }
        }.execute();
    }

    private MainInterface.ViewOpsForPresenter getView() {
        return mView.get();
    }

    private MainInterface.LoadViewOpsToPresenter getLoadView() {
        return mLoadView.get();
    }

    @Override
    public Context getAppContext() {
        return mContext;
//        return getView().getAppContext();
    }
}
