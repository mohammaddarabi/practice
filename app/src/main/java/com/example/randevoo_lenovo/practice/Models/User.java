package com.example.randevoo_lenovo.practice.Models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by randevoo on 9/11/2018.
 */

@Entity(tableName = "user")
public class User {

    @PrimaryKey(autoGenerate = true)
    public int Id;

    @ColumnInfo(name = "sha_1_name")
    public String digestName ;

    @ColumnInfo(name = "sha_1_user_name")
    public String digestUserName ;

    @ColumnInfo(name = "name")
    public byte[] name;

    @ColumnInfo(name = "birthDay")
    public byte[] userBirthDay;

    @ColumnInfo(name = "userName")
    public byte[] userName;

    @ColumnInfo(name = "emailAddress")
    public byte[] email;

    @ColumnInfo(name = "phoneNumber")
    public byte[] phone;

    public User(String digestName, String digestUserName, byte[] name, byte[] userBirthDay, byte[] userName, byte[] email, byte[] phone) {
        this.digestName = digestName ;
        this.digestUserName = digestUserName ;
        this.name = name;
        this.userBirthDay = userBirthDay;
        this.userName = userName;
        this.email = email;
        this.phone = phone;
    }

    public int getId() {
        return Id;
    }

    public String getDigestName() {
        return digestName;
    }

    public String getDigestUserName() {
        return digestUserName;
    }

    public byte[] getName() {
        return name;
    }

    public byte[] getUserBirthDay() {
        return userBirthDay;
    }

    public byte[] getUserName() {
        return userName;
    }

    public byte[] getEmail() {
        return email;
    }

    public byte[] getPhone() {
        return phone;
    }
}
