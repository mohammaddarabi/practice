package com.example.randevoo_lenovo.practice.Model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.randevoo_lenovo.practice.Models.Calendar;
import com.example.randevoo_lenovo.practice.Models.Day;
import com.example.randevoo_lenovo.practice.Models.DayDetail;
import com.example.randevoo_lenovo.practice.Models.Month;

import java.util.List;

/**
 * Created by Randevoo - lenovo on 10/24/2018.
 */

@Dao
public interface CalendarOps {


    //CalendarTables Select/Insert methods
    @Query("SELECT * FROM calendarTable")
    List<Calendar> getCalendar();

    @Insert
    long[] insertCalendar(Calendar... calendar);


    //MonthTable Select/Insert methods
    @Query("SELECT * FROM month")
    List<Month> getAllMonths();

    @Query("SELECT * FROM month WHERE yearKey LIKE :yearKEY")
    List<Month> getSpecMonths(int yearKEY);

    @Insert
    long[] insertMonth(Month... month);


    //DayTabke Select/Insert methods
    @Query("SELECT * FROM day")
    List<Day> getAllDays();

    @Query("SELECT * FROM day WHERE monthKey LIKE :monthKEY")
    List<Day> getSpecDays(int monthKEY);

    @Insert
    long[] insertDay(Day... day);


    //DayDetailTable Select/Insert methods
    @Query("SELECT * FROM dayDetails")
    List<DayDetail> getAllDayDetails();

    @Query("SELECT * FROM daydetails WHERE dayKey LIKE :dayKEY")
    List<DayDetail> getSpecDayDetail(int dayKEY);

    @Insert
    long[] insertDayDetail(DayDetail... dayDetail);
}
