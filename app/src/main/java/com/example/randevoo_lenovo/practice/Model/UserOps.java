package com.example.randevoo_lenovo.practice.Model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.randevoo_lenovo.practice.Models.User;

import java.util.List;


/**
 * Created by randevoo on 9/12/2018.
 */

@Dao
public interface UserOps {

    @Query("SELECT * FROM user ")
    List<User> getAllUsers() ;

    @Query("SELECT * FROM user WHERE sha_1_name LIKE :name AND sha_1_user_name LIKE :userName")
    User getUser(String name, String userName);

    @Insert
    long[] insertUser(User... mUser);
}
