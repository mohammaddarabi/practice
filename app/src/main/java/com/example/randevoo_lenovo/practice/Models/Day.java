package com.example.randevoo_lenovo.practice.Models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Randevoo - lenovo on 10/24/2018.
 */

@Entity(tableName = "day")
public class Day {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "dayID")
    public int dayID;

    @ForeignKey(entity = Month.class, parentColumns = "monthID", childColumns = "monthKey")
    @NonNull
    public int monthKey;

    @ColumnInfo(name = "dayName")
    public String dayName ;

    @ColumnInfo(name = "confirmedCount")
    public int confirmedCount;

    @ColumnInfo(name = "canceledCoount")
    public int canceledCoount ;

    @ColumnInfo(name = "waitingCount")
    public int waitingCount ;

    public Day(@NonNull int monthKey, String dayName, int confirmedCount, int canceledCoount,
               int waitingCount) {
        this.monthKey = monthKey;
        this.dayName = dayName;
        this.confirmedCount = confirmedCount;
        this.canceledCoount = canceledCoount;
        this.waitingCount = waitingCount;
    }

    public int getDayID() {
        return dayID;
    }

    @NonNull
    public int getForeignKey() {
        return monthKey;
    }

    public String getDayName() {
        return dayName;
    }

    public int getConfirmedCount() {
        return confirmedCount;
    }

    public int getCanceledCoount() {
        return canceledCoount;
    }

    public int getWaitingCount() {
        return waitingCount;
    }
}
