package com.example.randevoo_lenovo.practice.Model;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.randevoo_lenovo.practice.Models.Calendar;
import com.example.randevoo_lenovo.practice.Models.Day;
import com.example.randevoo_lenovo.practice.Models.DayDetail;
import com.example.randevoo_lenovo.practice.Models.Month;
import com.example.randevoo_lenovo.practice.Models.User;

/**
 * Created by randevoo on 9/12/2018.
 */

@Database(entities = {User.class, Calendar.class, Month.class, Day.class, DayDetail.class}, version = 1)
public abstract class DataBase extends RoomDatabase {

    abstract UserOps getUserOps();

    abstract CalendarOps getCalendarOps();
}
