package com.example.randevoo_lenovo.practice.Presenter;

import android.content.Context;
import android.os.Build;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;

import java.math.BigInteger;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Calendar;

import javax.crypto.Cipher;
import javax.security.auth.x500.X500Principal;

/**
 * Created by randevoo on 9/23/2018.
 */

public class SecretUtils {

    private String mAlias = "alias";
    private final String PROVIDER = "AndroidKeyStore";
    private final String TRANSFORMATION = "RSA/ECB/PKCS1Padding";
    private final String USES_ALGORITHM = KeyProperties.KEY_ALGORITHM_RSA;
    private final Context mContext;
    private KeyStore mKeyStore;

    public SecretUtils(Context mContext, String mAlias) throws Exception{
        this.mContext = mContext;
        this.mAlias = mAlias;
        mKeyStore = KeyStore.getInstance(PROVIDER);
        mKeyStore.load(null);
        getKeyPair();
    }

    private void getKeyPair() throws Exception {
        if(!mKeyStore.containsAlias(mAlias)) {
            KeyPair mKeyPair = getKeyGenerator(mContext).generateKeyPair();
            mKeyPair.getPublic();
            mKeyPair.getPrivate();
        }
    }

    private KeyPairGenerator getKeyGenerator(Context mContext) throws Exception {
        KeyPairGenerator mKeyPairGenerator = KeyPairGenerator.getInstance("RSA", PROVIDER);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2 &&
                Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            mKeyPairGenerator.initialize(getKeyGen(mContext));
        else
            mKeyPairGenerator.initialize(getKeyGenSpec());
        return mKeyPairGenerator;
    }

    /**
     * @return AlgorithmParameterSpec that uses for api more than 22
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    private KeyGenParameterSpec getKeyGenSpec() {
        return new KeyGenParameterSpec.Builder(mAlias,
                KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_ECB)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                .setRandomizedEncryptionRequired(false)
                .build();
    }

    /**
     * @return a AlgorithmParameterSpec for api between 18 and 23
     */
    private KeyPairGeneratorSpec getKeyGen(Context mContext) {
        Calendar end = Calendar.getInstance();
        end.add(Calendar.YEAR, 30);
        return new KeyPairGeneratorSpec.Builder(mContext)
                .setAlias(mAlias)
                .setSubject(new X500Principal("CN=" + mAlias))
                .setSerialNumber(BigInteger.ONE)
                .setStartDate(Calendar.getInstance().getTime())
                .setEndDate(end.getTime())
                .build();
    }

    /**
     * @return KeyStore.PrivateKeyEntry
     */
    private KeyStore.PrivateKeyEntry getExtractedKey() throws Exception {
        return (KeyStore.PrivateKeyEntry) mKeyStore.getEntry(mAlias, null);
    }

    /**
     * @return publicKey
     * retirves PrivateKeyEntry from keyStore and return public key from that
     */
    private PublicKey getPubKey() throws Exception {
        return getExtractedKey().getCertificate().getPublicKey();
    }

    /**
     * @return privateKey
     * retirves PrivateKeyEntry from keyStore and return private key from that
     */
    private PrivateKey getPriKey() throws Exception {
        return getExtractedKey().getPrivateKey();
    }

    /**
     * @param plainText
     * @return byteArray of encrypted String
     */
    public byte[] encrypt(String plainText) throws Exception {
        return getCipher(Cipher.ENCRYPT_MODE, getPubKey()).doFinal(plainText.getBytes());
    }

    /**
     * @param en
     * @return decrypted String
     */
    public String decrypt(byte[] en) throws Exception {
        return new String(getCipher(Cipher.DECRYPT_MODE, getPriKey()).doFinal(en));
    }

    /**
     * @return a cipher thar uses for encrypt and decrypt data
     * this cipher uses RSA cryptographic mode
     **/
    private Cipher getCipher(int modeFlag, Key mKey) throws Exception {
        Cipher mCipher = Cipher.getInstance(TRANSFORMATION, "AndroidOpenSSL");
        mCipher.init(modeFlag, mKey);
        return mCipher;
    }
}
