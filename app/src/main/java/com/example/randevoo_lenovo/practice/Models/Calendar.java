package com.example.randevoo_lenovo.practice.Models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Randevoo - lenovo on 10/24/2018.
 */

@Entity(tableName = "calendarTable")
public class Calendar {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "yearID")
    public int yearID ;

    @ColumnInfo(name = "yearCount")
    public String yearCount ;

    public Calendar(String yearCount) {
        this.yearCount = yearCount;
    }

    public int getYearID() {
        return yearID;
    }

    public String getYearCount() {
        return yearCount;
    }
}
