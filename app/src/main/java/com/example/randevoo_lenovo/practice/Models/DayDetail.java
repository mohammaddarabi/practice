package com.example.randevoo_lenovo.practice.Models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Randevoo - lenovo on 10/24/2018.
 */

@Entity(tableName = "dayDetails")
public class DayDetail {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "dayDetailsID")
    public int dayDetailsID;

    @ForeignKey(entity = Day.class, parentColumns = "dayID", childColumns = "dayKey")
    @NonNull
    public int dayKey ;

    @ColumnInfo(name = "confirmed")
    public String confirmed;

    @ColumnInfo(name = "unConfirmed")
    public String unConfirmed ;

    @ColumnInfo(name = "canceledByUser")
    public String canceledByUser ;

    @ColumnInfo(name = "canceledByMarket")
    public String canceledByMarket ;

    @ColumnInfo(name = "reminderTime")
    public String reminderTime ;

    @ColumnInfo(name = "visitTime")
    public String visitTime ;

    @ColumnInfo(name = "startVisitTime")
    public String startVisitTime ;

    @ColumnInfo(name = "endVisitTime")
    public String endVisitTime ;

    @ColumnInfo(name = "address")
    public String address ;

    @ColumnInfo(name = "personName")
    public String personName ;

    @ColumnInfo(name = "personUserName")
    public String personUserName ;

    @ColumnInfo(name = "biography")
    public String biography ;

    public DayDetail(@NonNull int dayKey, String confirmed, String unConfirmed, String canceledByUser,
                     String canceledByMarket, String reminderTime, String visitTime, String startVisitTime,
                     String endVisitTime, String address, String personName, String personUserName,
                     String biography) {
        this.dayKey = dayKey;
        this.confirmed = confirmed;
        this.unConfirmed = unConfirmed;
        this.canceledByUser = canceledByUser;
        this.canceledByMarket = canceledByMarket;
        this.reminderTime = reminderTime;
        this.visitTime = visitTime;
        this.startVisitTime = startVisitTime;
        this.endVisitTime = endVisitTime;
        this.address = address;
        this.personName = personName;
        this.personUserName = personUserName;
        this.biography = biography;
    }

    public int getDayDetailsID() {
        return dayDetailsID;
    }

    @NonNull
    public int getForeignKey() {
        return dayKey;
    }

    public String getConfirmed() {
        return confirmed;
    }

    public String getUnConfirmed() {
        return unConfirmed;
    }

    public String getCanceledByUser() {
        return canceledByUser;
    }

    public String getCanceledByMarket() {
        return canceledByMarket;
    }

    public String getReminderTime() {
        return reminderTime;
    }

    public String getVisitTime() {
        return visitTime;
    }

    public String getStartVisitTime() {
        return startVisitTime;
    }

    public String getEndVisitTime() {
        return endVisitTime;
    }

    public String getAddress() {
        return address;
    }

    public String getPersonName() {
        return personName;
    }

    public String getPersonUserName() {
        return personUserName;
    }

    public String getBiography() {
        return biography;
    }
}
