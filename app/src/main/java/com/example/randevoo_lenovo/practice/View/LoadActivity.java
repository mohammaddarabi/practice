package com.example.randevoo_lenovo.practice.View;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.randevoo_lenovo.practice.MainInterface.MainInterface;
import com.example.randevoo_lenovo.practice.Presenter.Presenter;
import com.example.randevoo_lenovo.practice.R;


public class LoadActivity extends AppCompatActivity implements View.OnClickListener,
        MainInterface.LoadViewOpsToPresenter{

    private EditText mName ;
    private EditText mUserName ;
    private TextView mPhone ;
    private TextView mBirthDay ;
    private TextView mFamilyName ;
    private Button mLoad ;
    private MainInterface.PresenterOpsForView mPresenter ;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load);
        initView() ;
        setUpMVP() ;
    }

    private void setUpMVP() {
        mPresenter = new Presenter(this) ;
    }

    private void initView() {
        mName = findViewById(R.id.edt_input_name);
        mUserName = findViewById(R.id.edt_input_pass);
        mPhone = findViewById(R.id.txt_phone);
        mBirthDay = findViewById(R.id.txt_birth_day);
        mFamilyName = findViewById(R.id.txt_family_name);
        mLoad = findViewById(R.id.btn_load_info);
        mLoad.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String name = mName.getText().toString() ;
        String userName = mUserName.getText().toString() ;
        mPresenter.getUser(name  , userName);
//        showToast(name);
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this , message , Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setText(String phone, String userBirthDay, String email) {
        mPhone.setText(phone);
        mBirthDay.setText(userBirthDay);
        mFamilyName.setText(email);
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }
}
