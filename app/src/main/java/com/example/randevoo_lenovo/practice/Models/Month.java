package com.example.randevoo_lenovo.practice.Models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Randevoo - lenovo on 10/24/2018.
 */

@Entity(tableName = "month")
public class Month {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "monthID")
    public int monthID ;

    @ColumnInfo(name = "yearKey")
    public int yearKey ;

    @ColumnInfo(name = "monthName")
    public String monthName ;

    public Month(int yearKey, String monthName) {
        this.yearKey = yearKey;
        this.monthName = monthName;
    }

    public int getMonthID() {
        return monthID;
    }

    public int getForeignKey() {
        return yearKey;
    }

    public String getMonthName() {
        return monthName;
    }
}
